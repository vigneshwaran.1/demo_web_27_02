package com.Testngcucumber;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Stepdefintions {

	static WebDriver driver = null;

	@Given("Open the browser")
	public void open_the_browser() {

		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

	@When("Click on Login button")
	public void click_on_login_button() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@When("Enter valid username{string} in the email textbox")
	public void enter_valid_username_in_the_email_textbox(String string) {
		driver.findElement(By.id("Email")).sendKeys(string);
	}

	@When("Enter valid Password{string} in the password field")
	public void enter_valid_password_in_the_password_field(String string) {
		driver.findElement(By.id("Password")).sendKeys(string);
	}

	@Then("Click on the Login button")
	public void click_on_the_login_button() {
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();
	}

	@Then("the home page has displayed with logged user")
	public void the_home_page_has_displayed_with_logged_user() {
		System.out.println("Users Home page has displayed");
	}

	/*
	 * @Then("Click the Logout button") public void click_the_logout_button() throws
	 * InterruptedException { Thread.sleep(10000);
	 * driver.findElement(By.linkText("Log out")).click(); }
	 */

	@Given("user has to click book category")
	public void user_has_to_click_book_category() {

		driver.findElement(By.xpath("(//a[contains(text(),'Books')])[1]")).click();

	}

	@Then("The book has to been sort")
	public void the_book_has_to_been_sort() {
		WebElement sort = driver.findElement(By.id("products-orderby"));
		Select s = new Select(sort);
		s.selectByVisibleText("Price: High to Low");

		driver.findElement(By.xpath("(//a[text()='Fiction'])[2]")).click();
		driver.findElement(By.id("add-to-cart-button-45")).click();

		

	}

	@Then("The mobile has added to the cart")
	public void the_mobile_has_added_to_the_cart() {

		driver.findElement(By.xpath("(//a[contains(text(),'Electronics')])[1]")).click();
		driver.findElement(By.xpath("(//a[contains(text(),'Cell phones')])[4]")).click();
		driver.findElement(By.xpath("(//a[text()='Smartphone'])[2]")).click();
		driver.findElement(By.id("add-to-cart-button-43")).click();
		
	}

	@Given("User has to select gift")
	public void user_has_to_select_gift() {
		driver.findElement(By.xpath("(//a[contains(text(),'Gift Cards')])[1]")).click();
	}

	@Then("The user should display  {int} per page")
	public void the_user_should_display_per_page(Integer int1) {
		WebElement display = driver.findElement(By.id("products-pagesize"));
		Select s3 = new Select(display);
		s3.selectByVisibleText("4");
	}

	@Then("user should capture the name and price of thr gift card")
	public void user_should_capture_the_name_and_price_of_thr_gift_card() {
		WebElement price = driver.findElement(By.xpath("(//a[contains(text(),'$5 Virtual Gift Card')])[1]"));
		String t1 = price.getText();

		System.out.println("Capture the price:" + t1);
	}

	@Then("Logout user")
	public void logot_user() {
		driver.findElement(By.xpath("//a[text()='Log out']")).click();
		driver.quit();
	}

}
