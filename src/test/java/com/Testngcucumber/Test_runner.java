package com.Testngcucumber;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/feature/Demoweb.feature",glue="com.Testngcucumber",
plugin = {"html:target/cucumber-reports"},monochrome = true)
public class Test_runner {

}

